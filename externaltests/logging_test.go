package externaltests

import (
	"testing"

	"bitbucket.org/ConsentSystems/logging"
	"bitbucket.org/ConsentSystems/logging/loggingfakes"
)

func TestLogs(t *testing.T) {
	lgCl := &loggingfakes.FakeLogger{}
	logging.SetLogger(lgCl)

	logging.Info(logging.Log{})

	if lgCl.InfoCallCount() != 1 {
		t.Errorf("We expected 1 call to info but got %d", lgCl.InfoCallCount())
	}

	logging.Debug(logging.Log{})

	if lgCl.DebugCallCount() != 1 {
		t.Errorf("We expected 1 call to debug but got %d", lgCl.DebugCallCount())
	}

	logging.Warning(logging.Log{})

	if lgCl.WarningCallCount() != 1 {
		t.Errorf("We expected 1 call to warning but got %d", lgCl.WarningCallCount())
	}

	logging.Error(logging.Log{})

	if lgCl.ErrorCallCount() != 1 {
		t.Errorf("We expected 1 call to error but got %d", lgCl.ErrorCallCount())
	}

	logging.Fatal(logging.Log{})

	if lgCl.FatalCallCount() != 1 {
		t.Errorf("We expected 1 call to fatal but got %d", lgCl.FatalCallCount())
	}

}
