package logging

// ExampleLevelZero tests standard logs with level 0
func ExampleSetLevel() {
	SetLevel(0)
	Info(Log{Code: 1, Status: "500", Detail: "Hello"})
	Debug(Log{Code: 2, Status: "500", Detail: "Hello"})
	Warning(Log{Code: 3, Status: "500", Detail: "Hello"})
	Error(Log{Code: 4, Status: "500", Detail: "Hello"})
	Fatal(Log{Code: 5, Status: "500", Detail: "Hello"})

	SetLevel(1)
	Info(Log{Code: 1, Status: "500", Detail: "Hello"})
	Debug(Log{Code: 2, Status: "500", Detail: "Hello"})
	Warning(Log{Code: 3, Status: "500", Detail: "Hello"})
	Error(Log{Code: 4, Status: "500", Detail: "Hello"})
	Fatal(Log{Code: 5, Status: "500", Detail: "Hello"})

	SetLevel(2)
	Info(Log{Code: 1, Status: "500", Detail: "Hello"})
	Debug(Log{Code: 2, Status: "500", Detail: "Hello"})
	Warning(Log{Code: 3, Status: "500", Detail: "Hello"})
	Error(Log{Code: 4, Status: "500", Detail: "Hello"})
	Fatal(Log{Code: 5, Status: "500", Detail: "Hello"})

	SetLevel(3)
	Info(Log{Code: 1, Status: "500", Detail: "Hello"})
	Debug(Log{Code: 2, Status: "500", Detail: "Hello"})
	Warning(Log{Code: 3, Status: "500", Detail: "Hello"})
	Error(Log{Code: 4, Status: "500", Detail: "Hello"})
	Fatal(Log{Code: 5, Status: "500", Detail: "Hello"})

	SetLevel(4)
	Info(Log{Code: 1, Status: "500", Detail: "Hello"})
	Debug(Log{Code: 2, Status: "500", Detail: "Hello"})
	Warning(Log{Code: 3, Status: "500", Detail: "Hello"})
	Error(Log{Code: 4, Status: "500", Detail: "Hello"})
	Fatal(Log{Code: 5, Status: "500", Detail: "Hello"})
	// Output:
	// {{1 Hello 500} info}
	// {{2 Hello 500} debug}
	// {{3 Hello 500} warning}
	// {{4 Hello 500} error}
	// {{5 Hello 500} fatal}
	// {{2 Hello 500} debug}
	// {{3 Hello 500} warning}
	// {{4 Hello 500} error}
	// {{5 Hello 500} fatal}
	// {{3 Hello 500} warning}
	// {{4 Hello 500} error}
	// {{5 Hello 500} fatal}
	// {{4 Hello 500} error}
	// {{5 Hello 500} fatal}
	// {{5 Hello 500} fatal}
}
