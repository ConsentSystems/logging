package logging

import "fmt"

const (
	//LevelInfo sends everything
	LevelInfo = iota
	//LevelDebug sends from debug below
	LevelDebug = iota
	//LevelWarning sends from warning below
	LevelWarning = iota
	//LevelError sends from error below
	LevelError = iota
	//LevelFatal sends from fatal below
	LevelFatal = iota
)

// Log is teh common log message interface
type Log struct {
	Code   int32
	Detail string
	Status string
}

type logWithLevel struct {
	Log
	Level string
}

var logCl Logger
var level int

// Logger is the logging main interface
type Logger interface {
	Info(msg Log) error
	Debug(msg Log) error
	Warning(msg Log) error
	Error(msg Log) error
	Fatal(msg Log) error
	SetLevel(lvl int)
}

// Info logs an info message
func Info(msg Log) error {
	if logCl != nil {
		logCl.Info(msg)
	} else {
		if level != LevelInfo {
			return nil
		}
		fmt.Println(logWithLevel{Log: msg, Level: "info"})
	}
	return nil
}

// Debug logs a debug message
func Debug(msg Log) error {
	if logCl != nil {
		logCl.Debug(msg)
	} else {
		if level > LevelDebug {
			return nil
		}
		fmt.Println(logWithLevel{Log: msg, Level: "debug"})
	}
	return nil
}

// Warning logs a warning message
func Warning(msg Log) error {
	if logCl != nil {
		logCl.Warning(msg)
	} else {
		if level > LevelWarning {
			return nil
		}
		fmt.Println(logWithLevel{Log: msg, Level: "warning"})
	}
	return nil
}

// Error logs an error message
func Error(msg Log) error {
	if logCl != nil {
		logCl.Error(msg)
	} else {
		if level > LevelError {
			return nil
		}
		fmt.Println(logWithLevel{Log: msg, Level: "error"})
	}
	return nil
}

// Fatal logs a fatal message
func Fatal(msg Log) error {
	if logCl != nil {
		logCl.Fatal(msg)
	} else {
		fmt.Println(logWithLevel{Log: msg, Level: "fatal"})
	}
	return nil
}

// SetLevel sets the log level for the standard logger.
func SetLevel(lvl int) {
	if logCl != nil {
		logCl.SetLevel(lvl)
	}
	level = lvl
}

// SetLogger sets the logger to use. It does not pick up the previously set level
func SetLogger(lgr Logger) {
	logCl = lgr
}
